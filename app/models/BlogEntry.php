<?php

class BlogEntry extends Eloquent {

    protected $table = "blog_entries";
    protected $fillable = array(
        'page_title',
        'page_content',
        'keywords',
        'description'
    );
    public static $validationRules = array(
        'page_title' => 'Required',
        'page_content' => 'Required',
        'keywords' => 'Required',
        'description' => 'Required',
    );
    public $timestamps = false;

    public function save(array $options = array()) {
        $this->publish_date = time();
        $this->slug = 123;
        return parent::save($options);
    }

}