<?php

Route::get('/', 'HomeController@showWelcome');

/**
 * WaterController
 */
Route::get('water/index', "WaterController@index");
Route::get('water/hello', "WaterController@hello");


/**
 * BlogEntriesController
 */
Route::get('blog', "BlogEntriesController@index");
Route::get('blog/show/{id}/{slug}', "BlogEntriesController@show");
/**
 * BlogEntriesAdminController
 */
Route::get('admin/blog', "BlogEntriesAdminController@index");
Route::get('admin/blog/create', "BlogEntriesAdminController@create");
Route::post('admin/blog/create', "BlogEntriesAdminController@createSubmit");
Route::get('admin/blog/update/{id}', "BlogEntriesAdminController@update");
Route::post('admin/blog/update/{id}', "BlogEntriesAdminController@updateSubmit");
Route::delete('admin/blog/delete/{id}', "BlogEntriesAdminController@delete");

