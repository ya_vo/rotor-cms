<?php

class BlogEntriesAdminController extends BaseController {

    protected $layout = 'layouts.admin';

    protected $viewsFolder = 'blogEntriesAdmin';

    public function index() {
        $blogEntries = BlogEntry::paginate(10);
        $this->layout->content = View::make($this->viewsFolder . '.index', array(
                    'blogEntries' => $blogEntries
        ));
    }

    public function create() {
        return View::make($this->viewsFolder . '.create');
    }

    public function createSubmit() {
        $validator = Validator::make(Input::all(), BlogEntry::$validationRules);

        if ( $validator->passes() ) {
            BlogEntry::create(Input::all());
            Session::flash('success', 'Your entry was saved');
            return Redirect::action(get_class($this) . '@index');
        }

        $blogEntry = new BlogEntry(Input::all());
        return View::make($this->viewsFolder . '.create', array(
                    'blogEntry' => $blogEntry,
                    'validator' => $validator
        ));
    }

    public function update($id) {
        try {
            $blogEntry = BlogEntry::findOrFail($id);
        } catch ( \Illuminate\Database\Eloquent\ModelNotFoundException $e ) {
            return App::abort(404);
        }
        return View::make($this->viewsFolder . '.update', array(
                    'blogEntry' => $blogEntry,
        ));
    }

    public function updateSubmit($id) {

        $validator = Validator::make(Input::all(), BlogEntry::$validationRules);

        if ( $validator->passes() ) {

            try {
                $blogEntry = BlogEntry::findOrFail($id);
            } catch ( \Illuminate\Database\Eloquent\ModelNotFoundException $e ) {
                return App::abort(404);
            }

            $blogEntry->fill(Input::all());

            $blogEntry->save();

            Session::flash('success', 'Your entry was updated');
            return Redirect::action(get_class($this) . '@index');
        }

        $blogEntry = new BlogEntry(Input::all());
        return View::make($this->viewsFolder . '.create', array(
                    'blogEntry' => $blogEntry,
                    'validator' => $validator
        ));
    }

    public function delete($id) {
        BlogEntry::destroy($id);
        Session::flash('success', 'Your entry was deleted');
        return Redirect::action(get_class($this) . '@index');
    }

}