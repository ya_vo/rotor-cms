<?php

class HomeController extends BaseController {

    public function showWelcome() {
        return View::make('hello', array('paramterOne' => 'woohoo'));
        //	
        //
        //Hey guys you may use IoC...
        //try this
        //App::make($controller)->{$action}();
        //Eg:
        //App::make('HomeController')->getIndex();
        //and you may also give params
        //App::make('HomeController')->getIndex($params);
    }

    public function index($paramterOne) {
        return "ParameterOne is: $paramterOne";
    }

}