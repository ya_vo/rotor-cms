<?php

class BlogEntriesController extends BaseController {

    protected $layout = 'layouts.default';

    public function index() {
        $blogEntries = BlogEntry::paginate(10);
        $this->layout->content = View::make('blogEntries.index', array('blogEntries' => $blogEntries));
    }

    public function show($id) {
        $entry = BlogEntry::find($id);
        $this->layout->content = View::make('blogEntries.singleEntry', array('entry' => $entry));
    }

}
