@extends('layouts/default')

@section('content')

<div class="posts">
    @foreach ($blogEntries as $entry)
    <div class="entry">
        <a href="{{ action('BlogEntriesController@show', array('id'=> $entry->id,'slug'=>$entry->slug))  }}" ><h1 style="font-size: 1.3em" > {{ $entry->page_title; }}</h1></a>


        <!-- Meta details -->
        <div class="meta">
            <i class="icon-calendar"></i> {{ date('Y-m-d', $entry->publish_date) }}
        </div>



        <p>{{ substr(strip_tags($entry->page_content), 0, 1250) . '...'; }}</p>
        <a href="{{ action('BlogEntriesController@show', array('id'=> $entry->id,'slug'=>$entry->slug))  }}" class="btn">Прочети пълната статия...</a>
        <div class="clearfix"></div>
    </div>
    @endforeach
</div>
<div>
    {{ $blogEntries->links() }}
</div>
@stop
