@extends('layouts/default')

@section('content')

<div class="row-fluid">
    <div class="span9 offset1">


        <h1 style="font-size: 2em" > {{ $entry->page_title }}</h1>
        <div >

            <h2 style="font-size: 1em">{{ $entry->page_title }}</h2>
            {{ $entry->page_content }}
            <br/>

        </div>

    </div>


</div>
@stop