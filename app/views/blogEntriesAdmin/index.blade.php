@extends('layouts/default')

@section('content')

<table class="table table-striped">
    @foreach ($blogEntries as $entry)
    <tr>
        <td>
            <a href="{{ action('BlogEntriesAdminController@update', array('id'=> $entry->id))  }}" >{{ $entry->page_title; }}</a>
        </td>
        <td>

            <a href="{{ action('BlogEntriesAdminController@delete', array('id'=> $entry->id)) }}" data-method="delete" data-confirm="Are you sure?">Delete this</a>

        </td>
    </tr>
    @endforeach
</table>
<div>
    {{ $blogEntries->links() }}
</div>
@stop

