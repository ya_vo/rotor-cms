@extends('layouts/admin')

@section('content')
<?
if ( !empty($blogEntry) ) {
    echo Form::model($blogEntry, array(
        'url' => URL::action('BlogEntriesAdminController@updateSubmit', array('id' => $blogEntry->id)),
        'class' => 'form form-horizontal'
    ));
} else {
    echo Form::open(array(
        'url' => URL::action('BlogEntriesAdminController@updateSubmit', array('id' => $id)),
        'class' => 'form form-horizontal'
    ));
}
?>
@include('_shared.validatorErrors')
@include('blogEntriesAdmin._form')

<div class="control-group">
    <div class="control-label">

    </div>
    <div class="controls">
        <?= Form::submit("Update"); ?>
    </div>
</div>


<?= Form::close() ?>
@stop