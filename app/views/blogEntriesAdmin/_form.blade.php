<div class="control-group">
    <div class="control-label">
        <?= Form::label('page_title', 'Page Title') ?>
    </div>
    <div class="controls">
        <?= Form::text('page_title') ?>
    </div>
</div>

<div class="control-group">
    <div class="control-label">
        <?= Form::label('keywords', 'Keywords') ?>
    </div>
    <div class="controls">
        <?= Form::text('keywords', NULL, array('class' => "textarea")) ?>
    </div>
</div>

<div class="control-group">
    <div class="control-label">
        <?= Form::label('description', 'Description') ?>
    </div>
    <div class="controls">
        <?= Form::textarea('description', NULL, array('class' => "textarea", 'style' => 'height: 50px')) ?>
    </div>
</div>

<div class="control-group">
    <div class="control-label">
        <?= Form::label('page_content', 'Page Content') ?>
    </div>
    <div class="controls">
        <?= Form::textarea('page_content', NULL, array('class' => "textarea")) ?>
    </div>
</div>