@extends('layouts/admin')

@section('content')
<?

if ( !empty($blogEntry) ) {
    echo Form::model($blogEntry, array('action' => 'BlogEntriesAdminController@createSubmit', 'class' => 'form form-horizontal'));
} else {
    echo Form::open(array('action' => 'BlogEntriesAdminController@createSubmit', 'class' => 'form form-horizontal'));
}
?>
@include('_shared.validatorErrors')
@include('blogEntriesAdmin._form')

<div class="control-group">
    <div class="control-label">

    </div>
    <div class="controls">
        <?= Form::submit("Create"); ?>
    </div>
</div>


<?= Form::close() ?>
@stop