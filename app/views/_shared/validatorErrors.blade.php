<?
if ( !empty($validator) ):
    ?>
    <div class="alert alert-error">
        <ul><?
        foreach ( $validator->messages()->all() as $message ) :
            echo "<li>".$message."</li>"."\n\r";
        endforeach;
        ?>
        </ul>
    </div>
    <?
endif;
?>