<!DOCTYPE html>
<html lang="bg">
    <head>


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Влизане в сайта - Сайт за запознанства - Magnetichno.com | запознанства | запознанство | сайтове за запознанства</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="А ти имаш ли си някого? Ела и намери човека за теб сега! Magnetichno.com е лесен за употреба напълно безплатен сайт за запознанства.">
        <meta name="keywords" content="сайт за запознанства, сайтове за запознанства, запознанства, запознанство">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700' rel='stylesheet' type='text/css'>
        <link href="http://magnetichno.com/style/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="http://magnetichno.com/style/font-awesome.css">
        <link rel="stylesheet" href="http://magnetichno.com/style/flexslider.css">
        <link href="http://magnetichno.com/style/style.css?v=a9" rel="stylesheet">
        <link href="http://magnetichno.com/style/blue.css?v=a9" rel="stylesheet">  
        <link href="http://magnetichno.com/style/bootstrap-responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="http://magnetichno.com/js/html5shim.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="http://magnetichno.com/static/favicon.ico">
        <script src="http://magnetichno.com/js/custom.js"></script>
        <script src="http://magnetichno.com/js/jquery.js"></script>
        <script src="http://magnetichno.com/js/bootstrap.js"></script>
        <script src="http://magnetichno.com/js/custom.js"></script>
        <script src="http://magnetichno.com/static/tiptip/jquery.tipTip.minified.js"></script>
        <link rel="stylesheet" href="http://magnetichno.com/static/tiptip/tipTip.css">
    </head>
    <body>
        <div id="sait_za_zapoznanstva">
            <h1>Сайт за запознанства</h1>
            <span class="sait_za_zapoznanstva">
                <a href="http://magnetichno.com/">Запознанства - Magnetichno.com</a>
                <img src="http://magnetichno.com/static/placeholders/sait-za-zapoznanstva.jpg" alt="Сайт за запознанства" />
            </span>    
        </div>
        <div class="navbar  navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container" style="width: 980px">


                    <a class="brand" href="http://magnetichno.com/">Magnetichno.com</a>


                    <div class = "nav-collapse collapse" id = "main-menu">
                        <ul class = "nav" id = "main-menu-left">

                        </ul>
                        <ul class="nav pull-right" id="main-menu-right">
                            <li>
                                <a href="http://magnetichno.com/users/login">Влизане</a>
                            </li>
                            <li>
                                <a href="http://magnetichno.com/users/registration">Регистрация</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>        <header></header>
        <div class="main-sheet1"></div>
        <div class="main-sheet2"></div>
        <div class="main-sheet3"></div>
        <div class="main">
            <div class="container-fluid" >
                @yield('content')
            </div>
        </div>



    </body>
</html>